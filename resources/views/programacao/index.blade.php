@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <div class="row hidden-xs">
                        <div class="col-md-2">
                            <h4 style="position: relative; top: 30px;">
                                <a href="{{route('programacao.index')}}" class="hidden-xs">Voltar</a>
                            </h4>
                        </div>
                        <div class="col-md-8">
                            <h1 class="text-center">Programação Festa de São Cristóvão</h1>
                        </div>
                    </div>
                    <h1 class="text-center hidden-md hidden-lg">Programação Festa de São Cristóvão</h1>
                </div>
                </div>
            <div class="col-md-12">
                <dl class="dl-horizontal">
                    @foreach($dados as $d)
                        @if($d->titulo)<h2 class="text-primary">{{$d->titulo}}</h2>@endif
                        @if($d->data)<h4>Data: <small>{{\Carbon\Carbon::parse($d->data)->format('d/m/Y')}}</small></h4>@endif
                        @if($d->horario)<h4>Horário: <small>{{$d->horario}}</small></h4>@endif
                        @if($d->celebrante)<h4>Presidente: <small>{{$d->celebrante}}</small></h4>@endif
                        @if($d->canticos)<h4>Encarregado dos cânticos: <small>{{$d->canticos}}</small></h4>@endif
                        @if($d->noiteiros)<h4>Noiteiros: <small>{!! $d->noiteiros !!}</small></h4>@endif
                        @if($d->observacao)<h4>Observação: <small>{{$d->observacao}}</small></h4>@endif
                        @if($d->orientacoes)
                            <hr>
                            {!! $d->orientacoes !!}
                        @endif
                        <hr>
                    @endforeach
                </dl>
            </div>
        </div>
    </div>
@endsection