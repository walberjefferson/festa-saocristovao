@extends('layouts.admin')

@section('title_header')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Usuários <small>Cadastrados</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> <a href="{{route('admin.index')}}">Dashboard</a> / Usuários
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
@endsection

@section('content')
    <table class="table table-bordered table-hover">
        <thead>
        <tr>
            <th>Nome</th>
            <th>E-mail</th>
            <th width="5%" nowrap>Ações</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td class="text-center">
                    <a href="{{route('admin.user.edit', ['id' => $user->id])}}" class="text-success"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection