@extends('layouts.admin')

@section('title_header')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Editando <small>{{ $dados->name }}</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> <a href="{{route('admin.index')}}">Dashboard</a> / Usuários
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
@endsection

@section('content')
    {!! Form::model($dados, ['method' => 'put', 'route' => ['admin.user.update', $dados]]) !!}

    <div class="form-group">
        {{ Form::label('name', 'Nome', ['class' => 'awesome']) }}
        {{ Form::text('name', null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('email', 'E-mail', ['class' => 'awesome']) }}
        {{ Form::text('email', null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('password', 'Senha', ['class' => 'awesome']) }}
        {{ Form::password('password', ['class' => 'form-control']) }}
    </div>

    {{ Form::submit('Salvar', ['class' => 'btn btn-primary']) }}

    {!! Form::close() !!}

@endsection
