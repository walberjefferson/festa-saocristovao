@extends('layouts.admin')

@section('title_header')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Editando <small> - {{ $dados->titulo }}</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> <a href="{{route('admin.index')}}">Dashboard</a> / Editar
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
@endsection

@section('content')
    {!! Form::model($dados, ['method' => 'post', 'route' => ['programacao.update', $dados]]) !!}

    <div class="form-group">
        {{ Form::label('titulo', 'Titulo', ['class' => 'awesome']) }}
        {{ Form::text('titulo', null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('data', 'Data', ['class' => 'awesome']) }}
        {{ Form::date('data', (empty($dados->data))? \Carbon\Carbon::now() : $dados->data, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('horario', 'Horário', ['class' => 'awesome']) }}
        {{ Form::text('horario', null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('celebrante', 'Celebrante', ['class' => 'awesome']) }}
        {{ Form::text('celebrante', null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('canticos', 'Cânticos', ['class' => 'awesome']) }}
        {{ Form::text('canticos', null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('noiteiros', 'Noiteiros', ['class' => 'awesome']) }}
        {{ Form::textarea('noiteiros', null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('observacao', 'Observação', ['class' => 'awesome']) }}
        {{ Form::text('observacao', null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('orientacoes', 'Orientações Ultimo dia', ['class' => 'awesome']) }}
        {{ Form::textarea('orientacoes', null, ['class' => 'form-control ckeditor']) }}
    </div>

    {{ Form::submit('Salvar', ['class' => 'btn btn-primary']) }}

    {!! Form::close() !!}

@endsection
