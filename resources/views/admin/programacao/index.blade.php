@extends('layouts.admin')

@section('title_header')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Listagem <small>Completa</small>
                <small class="pull-right" style="position: relative; top: 15px;">{{ count($dados) }} Resultados</small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> <a href="{{route('admin.index')}}">Dashboard</a> / Listagem
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
@endsection

@section('content')
    <table class="table table-bordered table-hover">
        <thead>
        <tr>
            <th>Titulo</th>
            <th>Celebrante</th>
            <th width="8%">Data</th>
            <th width="5%">Ações</th>
        </tr>
        </thead>
        <tbody>
        @foreach($dados as $d)
            <tr>
                <td>{{$d->titulo}}</td>
                <td>{{$d->celebrante}}</td>
                <td class="text-center">{{\Carbon\Carbon::parse($d->data)->format('d/m/Y')}}</td>
                <td class="text-center">
                    <a href="{{route('programacao.edit', ['id' => $d->id])}}" class="text-success"><i class="fa fa-pencil" aria-hidden="true"></i></a> |
                    <a href="{{route('programacao.destroy', ['id' => $d->id])}}" class="text-danger" onclick="javascript:if(!confirm('Deseja realmente excluir {{$d->titulo}}?'))return false;"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection