@extends('layouts.admin')

@section('title_header')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Editando Configurações
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> <a href="{{route('admin.index')}}">Dashboard</a> / Configurações
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
@endsection

@section('content')

    {!! Form::model($config, ['method' => 'post', 'route' => ['config.update', $config]]) !!}

    <div class="checkbox">
        <label>
            {{ Form::checkbox('status', '1', $config->status) }} Status
        </label>
    </div>

    <div class="form-group">
        {{ Form::label('aviso', 'Aviso', ['class' => 'awesome']) }}
        {{ Form::text('aviso', null, ['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{ Form::label('iframe', 'Iframe', ['class' => 'awesome']) }}
        {{ Form::textarea('iframe', null, ['class' => 'form-control']) }}
    </div>

    {{ Form::submit('Salvar', ['class' => 'btn btn-primary']) }}

    {!! Form::close() !!}

@endsection
