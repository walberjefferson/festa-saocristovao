@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="page-header">
                    <h1>Adicionar Configuração</h1>
                </div>
                {!! Form::open(['method' => 'post', 'route' => 'config.store']) !!}

                <div class="checkbox">
                    <label>
                        {{ Form::checkbox('status', '1', true) }} Status
                    </label>
                </div>

                <div class="form-group">
                    {{ Form::label('aviso', 'Aviso', ['class' => 'awesome']) }}
                    {{ Form::text('aviso', null, ['class' => 'form-control']) }}
                </div>

                <div class="form-group">
                    {{ Form::label('iframe', 'Iframe', ['class' => 'awesome']) }}
                    {{ Form::textarea('iframe', null, ['class' => 'form-control']) }}
                </div>

                {{ Form::submit('Salvar', ['class' => 'btn btn-primary']) }}

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
