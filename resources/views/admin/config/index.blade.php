@extends('layouts.admin')

@section('title_header')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Configurações
                <small>- @if(!empty($config))<a href="{{ route('config.edit', ['id' => $config->id]) }}">Editar</a>
                    -@endif
                    <a href="{{ route('config.add') }}">Adicionar</a></small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> <a href="{{route('admin.index')}}">Dashboard</a> / Configurações
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
@endsection

@section('content')
    @if(!empty($config))
        <div class="col-md-4">
            <p><b>Status: </b> @if($config->status) <span class="label label-success">Ativado</span> @else <span class="label label-danger">Desativado</span> @endif</p>
            <p><b>Aviso: </b>{{$config->aviso}}</p>
        </div>
        <div class="col-md-8">
            <div class="embed-responsive embed-responsive-16by9">
                {!! $config->iframe !!}
            </div>
            <hr>
            <a href="http://http://stmv2.srvstm.com/walberjefferson/walberjefferson/playlist.m3u8"><img src="http://playerv.srvstm.com/img/icones/img-icone-player-android.png" width="32" height="32" /></a>
            <a href="http://stmv2.srvstm.com/walberjefferson/walberjefferson/playlist.m3u8"><img src="http://playerv.srvstm.com/img/icones/img-icone-player-blackberry.png" width="32" height="32" /></a>
            <a href="http://stmv2.srvstm.com/walberjefferson/walberjefferson/playlist.m3u8"><img src="http://playerv.srvstm.com/img/icones/img-icone-player-iphone.png" width="32" height="32" /></a>
        </div>
    @endif
@endsection
