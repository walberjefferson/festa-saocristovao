@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <div class="row">
                        <div class="col-md-9">
                            <h1>@if(!empty($dados)) {{$dados->titulo}}
                                <small>- {{\Carbon\Carbon::parse($dados->data)->format('d/m/Y')}}</small> @endif</h1>
                        </div>
                        <div class="col-md-3">
                            <h4 class="pull-right" style="position: relative; top: 30px;">
                                <a href="{{route('programacao.listagem')}}">Programação Completa</a>
                            </h4>
                        </div>
                    </div>

                </div>
            </div>
            @if(!empty($config))

                <div class="col-md-7">
                    @if($config->status)
                        <div class="embed-responsive embed-responsive-16by9">
                            {!! $config->iframe !!}
                        </div>
                    @else
                        <div class="embed-responsive embed-responsive-16by9" style="background: #FFF url('/img/banner-festa.jpg'); width: 653px; height: 367px;">
                            <h1 class="text-right" style="position: relative; top: 140px; margin-right: 20px;">
                                <small style="color: #2c2c2c; font-size: 85%;">{{$config->aviso}}
                                </small>
                            </h1>
                        </div>
                    @endif
                  </div>
              @endif
                  <div class="col-md-5">
                      @if(!empty($dados))
                          @if($dados->horario)
                              <h2>Horário:
                                  <small>{{$dados->horario}}</small>
                              </h2>
                          @endif

                          @if($dados->celebrante)
                              <h3>Celebrante:
                                  <small>{{$dados->celebrante}}</small>
                              </h3>
                          @endif

                          @if($dados->canticos)
                              <h3>Encarregado dos cânticos:
                                  <small>{{$dados->canticos}}</small>
                              </h3>
                          @endif

                          @if($dados->noiteiros)
                              <h3>Noiteiros:
                                  <small>{{$dados->noiteiros}}</small>
                              </h3>
                          @endif

                          @if($dados->observacao)
                              <h3>Observação:
                                  <small>{{$dados->observacao}}</small>
                              </h3>
                          @endif
                      @else
                          <h3>
                              <small>Nenhuma programação para hoje!</small>
                          </h3>
                      @endif
                  </div>
                </div>
        </div>
@endsection
