<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Programacao extends Model
{
    protected $table = "programacoes";

    protected $casts = [];

    protected $fillable = [
        'titulo',
        'data',
        'horario',
        'celebrante',
        'canticos',
        'noiteiros',
        'observacao',
        'orientacoes',
    ];

    /*public function getDataAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->format('d/m/Y');
    }*/
}
