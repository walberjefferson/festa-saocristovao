<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $casts = [
        'status' => 'boolean',
    ];

    protected $fillable = [
        'status',
        'aviso',
        'iframe',
    ];

    /*public function setStatusAttribute($value)
    {
        //echo $value;
        if($value < 0){
            $this->attributes['status'] = false;
        }else{
            $this->attributes['status'] = true;
        }

    }

    public function getStatusAttribute($value)
    {
        //echo $value;
        /*if(empty($value)){
            $this->attributes['status'] = false;
        }
        $this->attributes['status'] = true;
    }*/
}
