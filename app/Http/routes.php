<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/',['as' => 'programacao.index', 'uses' => 'ProgramacoesController@index']);
Route::get('/programacao', ['as' => 'programacao.listagem', 'uses' => 'ProgramacoesController@listagem']);

Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function (){

    Route::resource('/user', 'UsersController', ['except' => ['destroy', 'create', 'store', 'show']]);

    Route::group(['prefix' => 'programacao', 'as' => 'programacao.'], function (){
        Route::get('/', ['as' => 'listagemAdmin', 'uses' => 'ProgramacoesController@listagemAdmin']);
        Route::get('/add', ['as' => 'add', 'uses' => 'ProgramacoesController@create']);
        Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'ProgramacoesController@edit']);
        Route::post('/store', ['as' => 'store', 'uses' => 'ProgramacoesController@store']);
        Route::post('/update/{id}', ['as' => 'update', 'uses' => 'ProgramacoesController@update']);
        Route::get('/destroy/{id}', ['as' => 'destroy', 'uses' => 'ProgramacoesController@destroy']);
    });

    Route::group(['prefix' => 'config', 'as' => 'config.'], function (){
        Route::get('/', ['as' => 'index', 'uses' => 'ConfigController@index']);
        Route::get('/add', ['as' => 'add', 'uses' => 'ConfigController@create']);
        Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'ConfigController@edit']);
        Route::post('/store', ['as' => 'store', 'uses' => 'ConfigController@store']);
        Route::post('/update/{id}', ['as' => 'update', 'uses' => 'ConfigController@update']);
    });

    Route::get('/', ['as' => 'admin.index', 'uses' => 'ProgramacoesController@listagemAdmin']);
});

Route::auth();