<?php

namespace App\Http\Controllers;

use App\Config;
use Illuminate\Http\Request;

use App\Http\Requests;

class ConfigController extends Controller
{
    public function index()
    {
        $config = Config::first();
        return view('admin.config.index', compact('config'));
    }

    public function create()
    {
        return view('admin.config.create');
    }

    public function store(Request $request)
    {
        //dd($request->all());
        Config::create($request->all());
        return redirect()->route('config.index');
    }

    public function edit($id)
    {
        $config = Config::findOrFail($id);
        return view('admin.config.edit', compact('config'));
    }

    public function update(Request $request, $id)
    {
        $status = $request->input('status');
        $aviso = $request->input('aviso');
        $iframe = $request->input('iframe');

        if (empty($status)) {
            $status = false;
        } else {
            $status = true;
        }
        $config = Config::findOrFail($id);
        $config->fill(['status' => $status, 'aviso' => $aviso, 'iframe' => $iframe])->save();
        return redirect()->route('config.index');
    }
}
