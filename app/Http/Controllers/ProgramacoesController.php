<?php

namespace App\Http\Controllers;

use App\Config;
use App\Programacao;
use Illuminate\Http\Request;
use App\Http\Requests;

class ProgramacoesController extends Controller
{
    public function index()
    {
        $config = Config::first();
        $dados = Programacao::where('data', '=', date('Y-m-d'))->first();
        if(empty($config) || empty($dados)){
            return redirect()->route('programacao.listagem');
        }
        return view('welcome', compact('dados', 'config'));
    }

    public function listagem()
    {
        $dados = Programacao::orderBy('data', 'asc')->get();
        return view('programacao.index', compact('dados'));
    }

    public function listagemAdmin()
    {
        $dados = Programacao::orderBy('data', 'asc')->get();
        return view('admin.programacao.index', compact('dados'));
    }

    public function create()
    {
        return view('admin.programacao.create');
    }

    public function store(Request $request)
    {
        Programacao::create($request->all());
        return redirect()->route('programacao.listagemAdmin');
    }

    public function edit($id)
    {
        $dados = Programacao::findOrFail($id);
        return view('admin.programacao.edit', compact('dados'));
    }

    public function update(Request $request, $id)
    {
        $programacao = Programacao::findOrFail($id);
        $programacao->fill($request->all())->save();
        return redirect()->route('programacao.listagemAdmin');
    }

    public function destroy($id)
    {
        Programacao::destroy($id);
        return redirect()->route('programacao.listagemAdmin');
    }
}
