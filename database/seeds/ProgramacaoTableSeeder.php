<?php

use Illuminate\Database\Seeder;

class ProgramacaoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Programacao::class)->times(15)->create();
    }
}
