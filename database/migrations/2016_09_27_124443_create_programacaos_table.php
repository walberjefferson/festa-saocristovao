<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramacaosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programacoes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->date('data');
            $table->string('horario');
            $table->string('celebrante');
            $table->string('canticos');
            $table->text('noiteiros');
            $table->string('observacao');
            $table->text('orientacoes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('programacoes');
    }
}
