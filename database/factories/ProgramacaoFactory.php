<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Programacao::class, function (Faker\Generator $faker) {
    return [
        'titulo' => $faker->name,
        'data' => $faker->date(),
        'horario' => $faker->time($format = 'H:i:s', $max = 'now'),
        'celebrante' => $faker->name,
        'noiteiros' => $faker->text(400),
        'observacao' => $faker->paragraph(),
    ];
});
